﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectile : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField] float speed =10f;
    [SerializeField] GameObject explosion;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.position = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var newPosition = rb.position + Vector2.right * speed * Time.fixedDeltaTime;
        rb.MovePosition(newPosition);

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            if (explosion != null)
            {
                var go = Instantiate(explosion, transform.position, Quaternion.identity);
                Destroy(go, 1f);
            }
            Destroy(collision.gameObject);
            LevelManager.instance.score++;
        }
        
    }
}
