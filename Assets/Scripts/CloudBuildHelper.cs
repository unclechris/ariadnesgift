﻿using UnityEngine;
using System.Collections.Generic;
using MiniJSON;
using TMPro;

public class CloudBuildHelper : MonoBehaviour
{
    public GameObject buildManifest;

    void Start()
    {
        TMP_Text myText = buildManifest.GetComponent<TMP_Text>();
        var manifest = (TextAsset)Resources.Load("UnityCloudBuildManifest.json");
        if (manifest != null)
        {
            var manifestDict = Json.Deserialize(manifest.text) as Dictionary<string, object>;
            foreach (var kvp in manifestDict)
            {
                // Be sure to check for null values!
                var value = (kvp.Value != null) ? kvp.Value.ToString() : "";
                var myLine = string.Format("Key: {0}, Value: {1}", kvp.Key, value);
                Debug.Log(myLine);
                myText.text += myLine+"\n";
            }
        }
    }
}