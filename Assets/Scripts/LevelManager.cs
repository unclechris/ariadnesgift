﻿using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;
    [SerializeField] private List<Transform> _spawnPoints = new List<Transform>();
    public int score;
    public int wave;
    public int spawnCount;
    public float _delay;
    float _nextSpawnTime;
    [SerializeField] private GameObject enemy;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

    }
        // Start is called before the first frame update
        void Start()
    {
        wave = 1;
        score = 0;
        StartWave();
    }

    private void StartWave()
    {
        spawnCount = wave * 3;
        _delay = 10f / spawnCount ;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (ReadyToSpawn)
        {
            if (spawnCount <= 0) 
            { 
                wave++; 
                StartWave(); 
            }
            _nextSpawnTime = Time.time + _delay;
            var go = Instantiate(enemy, ChooseRandomLocation(), Quaternion.identity, this.transform);            
            spawnCount--;
        }
    }
    bool ReadyToSpawn => Time.time >= _nextSpawnTime;
    Vector3 ChooseRandomLocation() => _spawnPoints[Random.Range(0, _spawnPoints.Count)].position;
}
