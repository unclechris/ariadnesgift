﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField] private float speed = 6f;
    [SerializeField] private float magnitude = 2;
    [SerializeField] private float frequency = 10f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        magnitude = magnitude * LevelManager.instance.wave;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 direction =  new Vector2(-1, Mathf.Sin(Time.time*frequency) * magnitude);
        var newPosition = rb.position + direction * speed * Time.fixedDeltaTime;
        rb.MovePosition(newPosition);
    }
}
