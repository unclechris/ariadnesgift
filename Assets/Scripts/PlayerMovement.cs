﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static GameControl;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour, IMovementActions, ICombatActions
{
    [SerializeField] private GameControl playerControls;
    [SerializeField] private float speed;
    [SerializeField] private Vector2 direction;
    [Space]
    [SerializeField] private GameObject projectile;
    [SerializeField] private float _delay = 1f;
    [SerializeField] private SpriteRenderer shieldAccess;
    [Range(0f,1f)]
    [SerializeField] private float shieldPercent;
    float _nextSpawnTime;
    private Vector2 moveVelocity;
    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Awake()
    {
        playerControls = new GameControl();
        playerControls.Movement.SetCallbacks(this);
        playerControls.Combat.SetCallbacks(this);
        rb = GetComponent<Rigidbody2D>();
    }
    private void OnEnable()
    {
        playerControls.Enable();
    }
    private void OnDisable()
    {
        playerControls.Disable();
    }
    private void FixedUpdate()
    {
        var newPosition = rb.position + moveVelocity * Time.fixedDeltaTime;
        if (newPosition.y>4f)
        {
            newPosition = new Vector2(rb.position.x, 4f);
            moveVelocity = moveVelocity * 0f;
        }
        if (newPosition.y < -4f)
        {
            newPosition= new Vector2(rb.position.x, -4f);
            moveVelocity = moveVelocity * 0f;
        }

        rb.MovePosition(newPosition);        
    }
    bool ReadyToFire => Time.time >= _nextSpawnTime;

    public void OnJump(InputAction.CallbackContext context)
    {
        // throw new System.NotImplementedException();
    }

    public void OnFire(InputAction.CallbackContext context)
    {
        if (ReadyToFire)
        {
            _nextSpawnTime = Time.time + _delay;
            var go = Instantiate(projectile, transform.position, Quaternion.identity);
            Destroy(go, 5f);
        }
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        direction = context.ReadValue<Vector2>();
        direction = new Vector2(0, direction.y);
        moveVelocity = direction.normalized * speed;
    }
    public void OnHit()
    {
        shieldPercent -= .33f;
        if (shieldPercent<= -.30f)
        {
            PlayerDeath();
        }
        else
        {
            Color shieldColor = shieldAccess.color;
            shieldColor = new Color(shieldAccess.color.r, shieldAccess.color.g, shieldAccess.color.b, shieldPercent);
        }
    }

    private void PlayerDeath()
    {
       // throw new NotImplementedException();
    }
}