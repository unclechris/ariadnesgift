using UnityEngine;
using UnityEngine.Audio;
using System;

public enum SoundType
{
	music,
	effect
}

[Serializable]
public class Sound {

	public string name;
    public SoundType soundType;
    public AudioClip clip;
	[Range(0, 1)]
	public float volume = .78f;
	[Range(.1f, 3)]
	public float pitch = 1.0f;

	[HideInInspector]
	public AudioSource audioSource;
	public bool loop;	
}
