using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
	public AudioMixer myAudioMixer;
	public static AudioManager Instance;
	public float OverallVol, SfxVol, MusicVol;
	public Sound[] Sounds;
	// Use this for initialization
	void Awake()
	{
		if (Instance == null)
			Instance = this;
		else
		{
			Destroy(gameObject);
			return;
		}
		LoadPrefs();
		InitSoundArray();

	}

    private void LoadPrefs()
    {
		OverallVol = GetVol("OverallVolume");
		myAudioMixer.SetFloat("OverallVolume", Mathf.Log10(OverallVol) * 20);
		MusicVol = GetVol("MusicVolume");
		myAudioMixer.SetFloat("MusicVolume", Mathf.Log10(MusicVol) * 20);
		SfxVol = GetVol("EffectVolume");
		myAudioMixer.SetFloat("EffectVolume", Mathf.Log10(SfxVol) * 20);

	}
	private float GetVol(string key)
    {
	 	return PlayerPrefs.GetFloat(key,1.0f);
    }
	public void SetVol(string prefValue, float newValue)
	{
		Console.WriteLine("AudioManager:" + prefValue + newValue);
		switch (prefValue)
		{
			case "OverallVolume":
				SetOverallVol(newValue);
				break;
			case "MusicVolume":
				SetMusicVol(newValue);
				break;
			case "EffectVolume":
				SetEffectVol(newValue);
				break;
			default:
				break;

		}
	}

    private void SetEffectVol(float newValue)
    {
		if (Application.isPlaying)
		{
			Console.WriteLine("Set Sfx" + Mathf.Log10(newValue) * 20);
			myAudioMixer.SetFloat("EffectVolume", Mathf.Log10(newValue) * 20);
			PlayerPrefs.SetFloat("EffectVolume", newValue);
		}
		SfxVol = GetVol("EffectVolume");
	}

    private void SetMusicVol(float newValue)
    {
		if (Application.isPlaying)
		{
			Console.WriteLine("Set Music" + Mathf.Log10(newValue) * 20);
			myAudioMixer.SetFloat("MusicVolume", Mathf.Log10(newValue) * 20);
			PlayerPrefs.SetFloat("MusicVolume", newValue);
		}
		MusicVol = GetVol("MusicVolume");
	}

    private void SetOverallVol(float newValue)
    {
		if (Application.isPlaying)
		{
			Console.WriteLine("Set Overall" + Mathf.Log10(newValue) * 20);
			myAudioMixer.SetFloat("OverallVolume", Mathf.Log10(newValue) * 20);
			PlayerPrefs.SetFloat("OverallVolume", newValue);
		}
		OverallVol = GetVol("OverallVolume");
	}
	


    private void InitSoundArray()
	{
		foreach (Sound s in Sounds)
		{
			s.audioSource = gameObject.AddComponent<AudioSource>();
			s.audioSource.clip = s.clip;
			s.audioSource.volume = s.volume;
			s.audioSource.pitch = s.pitch;
			s.audioSource.loop = s.loop;
			switch (s.soundType)
			{
				case SoundType.music:
					s.audioSource.outputAudioMixerGroup = myAudioMixer.FindMatchingGroups("Music")[0];
					break;
				case SoundType.effect:
					s.audioSource.outputAudioMixerGroup = myAudioMixer.FindMatchingGroups("Effect")[0];
					break;
				default:
					break;
			}
		}
	}

	public void Play(string name)
	{
		Sound s = Array.Find(Sounds, sound => sound.name == name);
		if (s != null)
		{
			var curVolume = s.volume;
			if (s.audioSource.volume > 0)
			{
				s.audioSource.Play();
			}
		}
	}
	public void SavePrefs()
    {
		PlayerPrefs.Save();
    }
	private void Start()
	{
		Play("theme");
	}

}
