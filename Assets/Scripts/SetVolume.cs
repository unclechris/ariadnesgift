﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SetVolume : MonoBehaviour
{
    public Slider slider;
    public string Value;


    void Start()
    {
        slider.value = PlayerPrefs.GetFloat(Value, 0.75f);
    }
    public void SetLevel(float sliderValue)
    {
        AudioManager.Instance.SetVol(Value, sliderValue);
        //PlayerPrefs.SetFloat(Value, sliderValue);
    }
}