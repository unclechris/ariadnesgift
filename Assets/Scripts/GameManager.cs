﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public GameObject title;
	public GameObject version;
	public GameObject TitleScreen;
	public GameObject OptionScreen;
	public GameObject PauseScreen;

	public static GameManager instance;
	void Awake()
	{
		if (instance == null)
        {
            instance = this;
        }
        else
		{
			Destroy(gameObject);
			return;
		}
		var myTitleText = title.GetComponent<TMP_Text>();
		myTitleText.text = Application.productName;
		var myVersionText = version.GetComponent<TMP_Text>();
		myVersionText.text = "Ver "+Application.version;
		TitleScreen.SetActive(true);
		OptionScreen.SetActive(false);
		PauseScreen.SetActive(false);
	}

	public void QuitGame()
	{
		Application.Quit();
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#endif
	}
	public void StartGame()
    {
		SceneManager.LoadScene(1);
	}
	public void ReloadScene()
	{
		SceneManager.LoadScene(0);
	}

}
